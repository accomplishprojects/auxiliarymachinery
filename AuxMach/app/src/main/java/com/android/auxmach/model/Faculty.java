package com.android.auxmach.model;

/**
 * Created by luigigo on 3/16/16.
 */
public class Faculty {
    String accesscode;

    public String getAccesscode() {
        return accesscode;
    }

    public void setAccesscode(String accesscode) {
        this.accesscode = accesscode;
    }
}
