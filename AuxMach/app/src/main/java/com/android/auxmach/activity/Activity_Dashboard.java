package com.android.auxmach.activity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.FrameLayout;

import com.android.auxmach.R;
import com.android.auxmach.fragments.Fragment_About;
import com.android.auxmach.fragments.objects.BilgePumpObjectFragment;
import com.android.auxmach.fragments.topics.Fragment_Dashboard;
import com.android.auxmach.fragments.topics.Fragment_Topics;
import com.android.auxmach.utilities.tools.CustomTypefaceSpan;
import com.android.auxmach.utilities.tools.CustomUtils;

public class Activity_Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    FrameLayout frame_container;
    MenuItem mi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
//        setSupportActionBar(toolbar);
        initVars();
        toolbar.setTitle("Home");

    }

    private void initVars() {

        //Setting up drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        frame_container = (FrameLayout) findViewById(R.id.frame_content);
        CustomUtils.loadFragment(new Fragment_Dashboard(), true, this);
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();

            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
//                    Log.i("j Menu", String.valueOf(j));
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);

        }

    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.frame_content);
        FragmentManager fm = getSupportFragmentManager();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragment instanceof Fragment_Topics) {
                if (Fragment_Topics.mUnfoldableView.isUnfolded()) {
                    Fragment_Topics.mUnfoldableView.foldBack();
                } else {

                    for (int i = 1; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                        if (Fragment_Topics.isSpeaking) {
                            Fragment_Topics.myTTS.stop();
                        }

                    }
                }
            } else {
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack();
                } else if (fm.getBackStackEntryCount() == 1) {
                    new AlertDialog.Builder(this)
                            .setTitle("Logout?")
                            .setMessage("Are you sure you want to logout?")
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Activity_Dashboard.this.finish();
                                }
                            }).create().show();
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            CustomUtils.loadFragment(new Fragment_Dashboard(), true, this);
            toolbar.setTitle("Home");
            // Handle the camera action
        } else if (id == R.id.nav_cube) {
            CustomUtils.loadFragment(new BilgePumpObjectFragment(), true, this);
            toolbar.setTitle("3D Objects");
        } else if (id == R.id.nav_topics) {
            CustomUtils.loadFragment(new Fragment_Topics(), true, this);
            toolbar.setTitle("Topics");
        } else if (id == R.id.nav_about) {
            CustomUtils.loadFragment(new Fragment_About(), true, this);
            toolbar.setTitle("About");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface boldFont = Typeface.createFromAsset(getAssets(), "fonts/ironman.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        CharSequence menuTitle = mi.getTitle();
        if (mi.toString().contains("General")) {
            mNewTitle.setSpan(new CustomTypefaceSpan("", boldFont), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        } else {
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        }
    }

}
