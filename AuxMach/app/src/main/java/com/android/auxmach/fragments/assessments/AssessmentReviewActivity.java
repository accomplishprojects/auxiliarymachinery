package com.android.auxmach.fragments.assessments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.auxmach.R;

/**
 * Created by vidalbenjoe on 10/03/2016.
 */
public class AssessmentReviewActivity extends AppCompatActivity {
    int answerPos;
    TextView solutionTxt;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.assessment_review_layout);
        toolbar = (Toolbar) findViewById(R.id.reviewquiztoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Review");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        answerPos = getIntent().getExtras().getInt("item");
        Log.i("answerpost:", "" + answerPos);
        solutionTxt = (TextView) findViewById(R.id.solutionTxt);
        if (answerPos == 1) {
            solutionTxt.setText(getString(R.string.solution1_text));
        } else if (answerPos == 2) {
            solutionTxt.setText(getString(R.string.solution2_text));
        } else if (answerPos == 3) {
            solutionTxt.setText(getString(R.string.solution3_text));
        } else if (answerPos == 4) {
            solutionTxt.setText(getString(R.string.solution4_text));
        } else if (answerPos == 5) {
            solutionTxt.setText(getString(R.string.solution5_text));
        } else if (answerPos == 6) {
            solutionTxt.setText(getString(R.string.solution6_text));
        } else if (answerPos == 7) {
            solutionTxt.setText(getString(R.string.solution7_text));
        } else if (answerPos == 8) {
            solutionTxt.setText(getString(R.string.solution8_text));
        } else if (answerPos == 9) {
            solutionTxt.setText(getString(R.string.solution9_text));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                this.finish();
                overridePendingTransition(R.anim.slide_in_left,
                        R.anim.slide_out_left);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        AssessmentReviewActivity.this.finish();
        super.onBackPressed();
    }

}
