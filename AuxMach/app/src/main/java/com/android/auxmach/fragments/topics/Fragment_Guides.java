package com.android.auxmach.fragments.topics;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.android.auxmach.R;

/**
 * Created by luigigo on 3/13/16.
 */
public class Fragment_Guides extends Fragment {

    View mainView;
    public static int page = 0;
    public static String strHeader, strDescription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_guides, container, false);

        ViewFlipper flipper = (ViewFlipper) mainView.findViewById(R.id.flipper);
        TextView txtHeader = (TextView) mainView.findViewById(R.id.txtHeader);
        TextView txtDescription = (TextView) mainView.findViewById(R.id.txtDescription);

        flipper.setDisplayedChild(page);
        txtHeader.setText(strHeader);
        txtDescription.setText(strDescription);
        return mainView;
    }
}
