package com.android.auxmach.fragments.assessments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.auxmach.R;
import com.android.auxmach.database.AssessmentDB.QUIZDBAdapter;
import com.android.auxmach.database.AssessmentDB.Question;
import com.android.auxmach.database.AssessmentDB.SessionCache;
import com.android.auxmach.database.AssessmentDB.TempQuestion;
import com.android.auxmach.utilities.tools.CustomUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by vidalbenjoe on 28/02/2016.
 */
public class AuxMachAssesmentActivity extends AppCompatActivity {
    List<Question> quesList;
    public Question question;

    SessionCache QuizSession;
    QUIZDBAdapter myDb;

    int score;
    int qid = 0;
    int page = 1;
    int qset = 1;

    String Uname;
    String ncourse;
    String finalDate;
    private Date date;

    ImageView quizlin;
    TextView tvQue, tvPage, tvRef;
    RadioButton rd1, rd2, rd3, rd4;
    Button bnext;
    String questionCategory;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_main_layout);

        toolbar = (Toolbar) findViewById(R.id.quiztoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomUtils.getQuestionCategory() + " Assessment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        QuizSession = new SessionCache(getApplicationContext());
        openDB();
        myDb.delAllTempRows();
        quesList = myDb.getAllQuestionsHeatEngine();
        Log.i("category:", CustomUtils.getQuestionCategory());
        ncourse = CustomUtils.getQuestionCategory();
        questionCategory();
        if (quesList.size() == 0) {
            loadQuestion();
            questionCategory();
        } else {
            Log.d("database not empty", "queslist with setno");
        }
        Typeface roadBrushttf = Typeface.createFromAsset(getAssets(), "fonts/ironman.ttf");
        quizlin = (ImageView) findViewById(R.id.quizlin);
        question = quesList.get(qid);
        tvRef = (TextView) findViewById(R.id.tvRef);
        tvQue = (TextView) findViewById(R.id.tvQuestion);
        tvPage = (TextView) findViewById(R.id.tvPage);
        rd1 = (RadioButton) findViewById(R.id.rd1);
        rd2 = (RadioButton) findViewById(R.id.rd2);
        rd3 = (RadioButton) findViewById(R.id.rd3);
        rd4 = (RadioButton) findViewById(R.id.rd4);
        bnext = (Button) findViewById(R.id.bnext);


        bnext.setEnabled(false);
        tvQue.setTypeface(roadBrushttf);
        rd1.setTypeface(roadBrushttf);
        rd2.setTypeface(roadBrushttf);
        rd3.setTypeface(roadBrushttf);
        rd4.setTypeface(roadBrushttf);
        bnext.setTypeface(roadBrushttf);

        setQuestionView();
        tvPage.setText(page + "");
        Log.d("score result", "Randomized Num " + tvRef.getText().toString());
        rd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnext.setEnabled(true);
            }
        });

        rd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnext.setEnabled(true);
            }
        });

        rd3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnext.setEnabled(true);
            }
        });

        rd4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bnext.setEnabled(true);
            }
        });

        bnext.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {
                RadioGroup grp = (RadioGroup) findViewById(R.id.radioGroup1);
                RadioButton answer = (RadioButton) findViewById(grp
                        .getCheckedRadioButtonId());
                grp.clearCheck();
                page++;

                if (question.getQans().equals(answer.getText())) {
                    String lid = tvRef.getText().toString();
                    String qitem = tvQue.getText().toString();
                    String qans = question.getQans().toString();
                    String quserans = answer.getText().toString();
                    myDb.addtempQuestion(new TempQuestion("1", lid, qitem,
                            qans, quserans));
                    score++;
                    Log.d("course", "Your score is " + score);
                } else if (!question.getQans().equals(answer.getText())) {
                    String lid = tvRef.getText().toString();
                    String qitem = tvQue.getText().toString();
                    String qans = question.getQans().toString();
                    String quserans = answer.getText().toString();
                    myDb.addtempQuestion(new TempQuestion("1", lid, qitem,
                            qans, quserans));
                    Log.d("wrong answer", answer.getText() + " is incorrect");
                }
                if (qid < 5) {
                    question = quesList.get(qid);
                    setQuestionView();
                    grp.clearCheck();
                    tvPage.setText(page + "");
                    bnext.setEnabled(false);
                } else {
                    UserDialog();
                }
            }
        });
    }

    public void UserDialog() {

        final Dialog dialog = new Dialog(AuxMachAssesmentActivity.this,
                R.style.DialogAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_entername_layout);
        dialog.setCancelable(false);
        final EditText enterNameEdt = (EditText) dialog.findViewById(R.id.enterNameEdt);
        Button submitNameBtn = (Button) dialog.findViewById(R.id.submitNameBtn);

        submitNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Uname = enterNameEdt.getText().toString();
                Intent in = getIntent();
                int retake = in.getExtras().getInt("retakeNum");
                date = new Date();
                SimpleDateFormat timeFormat = new SimpleDateFormat(
                        "MMM dd, yyyy");
                finalDate = timeFormat.format(date);
                String subj = ncourse + " " + qset;
                String qdetails = "Retake: " + retake;
                myDb.addscores(3, retake, subj, qdetails, score, finalDate, Uname);
                myDb.deleteQuiz(ncourse);
                Intent intent = new Intent(getApplicationContext(), AssessmentResultsActivity.class);
                Bundle b = new Bundle();
                b.putInt("qno", qset);
                b.putInt("score", score);
                Log.i("PASS FINAL SCORE %d: ", String.valueOf(score));
                b.putString("course", ncourse);//
                b.putString("quizdetails", qdetails);
                intent.putExtras(b);
                startActivity(intent);
                AuxMachAssesmentActivity.this.finish();
                closeDB();
            }
        });

        dialog.show();
    }

    public void loadQuestion() {
        if (CustomUtils.getQuestionCategory().contentEquals("Air Distribution")) {
            AirDistributionQues();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Air Compressor")) {
            AirCompQues();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Construction Details of Reciprocating Compressors")) {
            ReciprocatingCompressorQuestion();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Heat Engine Cycle")) {
            heatEngineQuestion();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Heat Exchanger")) {
            heatExchangerQuestion();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Storage of Compressed Air")) {
            storageCompressAirQuestion();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Operate Pumping Systems and Associated Control Systems")) {
            operatingPumpingQuestion();
        }

    }

    public void questionCategory() {
        if (CustomUtils.getQuestionCategory().contentEquals("Air Distribution")) {
            quesList = myDb.getAllQuestionsAIRDIST();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Air Compressor")) {
            quesList = myDb.getAllQuestionsAirCompress();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Construction Details of Reciprocating Compressors")) {
            quesList = myDb.getAllQuestionsReciptrocate();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Heat Engine Cycle")) {
            quesList = myDb.getAllQuestionsHeatEngine();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Heat Exchanger")) {
            quesList = myDb.getAllQuestionsHeatExchange();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Storage of Compressed Air")) {
            quesList = myDb.getAllQuestionsStorageCompressAir();
        } else if (CustomUtils.getQuestionCategory().contentEquals("Operate Pumping Systems and Associated Control Systems")) {
            quesList = myDb.getAllQuestionsOperatePumpSys();
        }

    }

    public void setQuestionView() {
        tvRef.setText(question.getLid());
        tvQue.setText(question.getQitem());
        rd1.setText(question.getOpta());
        rd2.setText(question.getOptb());
        rd3.setText(question.getOptc());
        rd4.setText(question.getOptd());

        if (rd3.getText().toString().isEmpty() && rd4.getText().toString().isEmpty()) {
            rd3.setVisibility(View.INVISIBLE);
            rd4.setVisibility(View.INVISIBLE);
        } else {
            if (rd3.getText().toString().isEmpty()) {
                rd3.setVisibility(View.INVISIBLE);
            } else if (rd4.getText().toString().isEmpty()) {
                rd4.setVisibility(View.INVISIBLE);
            } else {
                rd3.setVisibility(View.VISIBLE);
                rd4.setVisibility(View.VISIBLE);
            }
        }
        String refNumber = tvRef.getText().toString();
        int refInt = Integer.parseInt(refNumber);

        if (CustomUtils.getQuestionCategory().contentEquals("Air Compressor")) {
            if (refInt == 9) {//
                quizlin.setBackgroundResource(R.drawable._banner_reciprocating_air_compressor);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 10) {
                quizlin.setBackgroundResource(R.drawable.banner_rotary_screw_air_compressor);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 11) {
                quizlin.setBackgroundResource(R.drawable.rotary_scroll_compressor);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 12) {
                quizlin.setBackgroundResource(R.drawable.banner_aircompressor);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 13) {
                quizlin.setBackgroundResource(R.drawable.banner_rotary_sliding_vain);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 14) {
                quizlin.setBackgroundResource(R.drawable.banner_centrifugal);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 15) {
                quizlin.setBackgroundResource(R.drawable.banner_rockingpiston1);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 16) {
                quizlin.setBackgroundResource(R.drawable.banner_multi_stage_compressor);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 17) {
                quizlin.setBackgroundResource(R.drawable.banner_aftercooler);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 18) {
                quizlin.setBackgroundResource(R.drawable.banner_intercooler);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else {
                quizlin.setVisibility(View.GONE);
            }
        } else if (CustomUtils.getQuestionCategory().contentEquals("Air Distribution")) {
            if (refInt == 11) {//
                quizlin.setBackgroundResource(R.drawable.banner_centrifugal_fan);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 12) {
                quizlin.setBackgroundResource(R.drawable.banner_axial_fan);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 13) {
                quizlin.setBackgroundResource(R.drawable.banner_fans);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 14) {
                quizlin.setBackgroundResource(R.drawable.tubeaxial);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 15) {
                quizlin.setBackgroundResource(R.drawable.propellerimg);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            }else {
                quizlin.setVisibility(View.GONE);
            }
        } else if (CustomUtils.getQuestionCategory().contentEquals("Construction Details of Reciprocating Compressors")) {
            if (refInt == 11) {//
                quizlin.setBackgroundResource(R.drawable.bannner_cylinder_block);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 12) {
                quizlin.setBackgroundResource(R.drawable.banner_cylinder_head);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 13) {
                quizlin.setBackgroundResource(R.drawable.banner_piston);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 14) {
                quizlin.setBackgroundResource(R.drawable.banner_piston_rings);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 15) {
                quizlin.setBackgroundResource(R.drawable.banner_connecting_rod);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 16) {
                quizlin.setBackgroundResource(R.drawable.banner_gudgeon);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 17) {
                quizlin.setBackgroundResource(R.drawable.banner_safety_valve);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 18) {
                quizlin.setBackgroundResource(R.drawable.banner_suction);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 19) {
                quizlin.setBackgroundResource(R.drawable.banner_discharge_valve);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            }else {
                quizlin.setVisibility(View.GONE);
            }
        } else if (CustomUtils.getQuestionCategory().contentEquals("Storage of Compressed Air")) {
            if (refInt == 11) {//
                quizlin.setBackgroundResource(R.drawable.banner_spring_loaded_relief_valves);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 12) {
                quizlin.setBackgroundResource(R.drawable.springpressurevalve);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 13) {
                quizlin.setBackgroundResource(R.drawable.banner_inlet_valves);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 14) {
                quizlin.setBackgroundResource(R.drawable.banner_bursting_disc);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            } else if (refInt == 15) {
                quizlin.setBackgroundResource(R.drawable.banner_drain_plug);
                quizlin.setVisibility(View.VISIBLE);
                Log.d("REFERENCE NUMBER:", tvRef.getText().toString());
            }else {
                quizlin.setVisibility(View.GONE);
            }
        } else if (CustomUtils.getQuestionCategory().contentEquals("Heat Engine Cycle")) {

        }
        qid++;
    }


    public void AirCompQues() {
        myDb.AirCompQuestion(new Question(
                "1",
                "This type of compressor has a motor that consists of a rotor, stator, and blades. On one end there is an intake port, and on the other an exhaust port.",
                "ROTARY SLIDING VANE AIR COMPRESSOR",
                "CENTRIFUGAL AIR COMPRESSOR",
                "ROTARY SLIDING VANE AIR COMPRESSOR",
                "ROCKING PISTON  COMPRESSOR",
                ""));//1

        myDb.AirCompQuestion(new Question(
                "2",
                "This compressor is a variation of the standard piston-type unit although in this case, the air pressure is increased through a reciprocating interaction of a connecting rod and piston.",
                "ROCKING PISTON  COMPRESSOR",
                "CENTRIFUGAL AIR COMPRESSOR",
                "ROTARY SLIDING VANE AIR COMPRESSOR",
                "ROCKING PISTON  COMPRESSOR",
                ""));

        myDb.AirCompQuestion(new Question(
                "3",
                "A series of cylinders, each of a different diameter. Between each compression stage, the air passes through a heat exchanger, where it is cooled.",
                "MULTI-STAGE COMPRESSOR",
                "CENTRIFUGAL AIR COMPRESSOR",
                "ROTARY SLIDING VANE AIR COMPRESSOR",
                "MULTI-STAGE COMPRESSOR",
                ""));

        myDb.AirCompQuestion(new Question(
                "4",
                "As the air is compressed by a turbo/supercharger it gets very hot, very quickly. As its temperature climbs, its oxygen content (density) drops, so by cooling the air, an intercooler provides a denser, more oxygen rich air to the engine thus improving the combustion by allowing more fuel to be burned.",
                "AFTERCOOLER",
                "INTERCOOLER",
                "AFTERCOOLER",
                "PISTON ",
                ""));

        myDb.AirCompQuestion(new Question(
                "5",
                "This type of compressor has two mated helical screws located inside a chamber. As each of the two screws rotates, the air pressure inside the chamber is increased which effectively reduces the volume of air.",
                "ROTARY SCREW COMPRESSOR",
                "ROTARY SCREW COMPRESSOR",
                "AFTERCOOLER",
                "ROCKING PISTON COMPRESSOR ",
                ""));

        myDb.AirCompQuestion(new Question(
                "6",
                "This type of compressor has two mated helical screws located inside a chamber. As each of the two screws rotates, the air pressure inside the chamber is increased which effectively reduces the volume of air.",
                "ROTARY SCREW COMPRESSOR",
                "ROTARY SCREW COMPRESSOR",
                "AFTERCOOLER",
                "ROCKING PISTON COMPRESSOR ",
                ""));

        myDb.AirCompQuestion(new Question(
                "7",
                "It takes the air in from the atmosphere just like single-stage units, but as you can see on the picture they have two cylinders instead of one.",
                "TWO- STAGE COMPRESSOR",
                "TWO- STAGE COMPRESSOR",
                "AFTERCOOLER",
                "ROCKING PISTON COMPRESSOR ",
                ""));

        myDb.AirCompQuestion(new Question(
                "8",
                "When the piston moves down inside the cylinder, the air enters the space above thanks to the difference between the atmospheric pressure and the pressure inside the cylinder. The air is then compressed as the piston moves upwards. These are known as:",
                "SINGLE- STAGE COMPRESSOR",
                "TWO- STAGE COMPRESSOR",
                "SINGLE- STAGE COMPRESSOR",
                "ROCKING PISTON COMPRESSOR ",
                ""));


        ///Images question
        myDb.AirCompQuestion(new Question(
                "9",
                "Identify the image",
                "Double-stage recompressions",
                "Double-stage recompressions",
                "Single-stage compressors.",
                "Triple-stage compressors",
                "Two-stage compressors"));//_banner_reciprocating_air_compressor


        myDb.AirCompQuestion(new Question(
                "10",
                "Identify the image",
                "Rotary Screw Air Compressors",
                "Rotary Screw Air Compressors",
                "Single-Piston compressors.",
                "Rotary Scroll Air Compressors",
                "Two-stage compressors"));//banner_rotary_screw_air_compressor

        myDb.AirCompQuestion(new Question(
                "11",
                "Identify the image",
                "Rotary Scroll Air Compressors.",
                "Rotary Screw Air Compressors",
                "Single-Piston compressors.",
                "Rotary Scroll Air Compressors",
                "Two-stage compressors"));//rotary_scroll_compressor

        myDb.AirCompQuestion(new Question(
                "12",
                "Identify the image",
                "Air Compressors",
                "Air Compressors",
                "Water compressors",
                "Rotary Scroll Air Compressors",
                "Two-rotator compressors"));//banner_aircompressor

        myDb.AirCompQuestion(new Question(
                "13",
                "Identify the image",
                "Rotary Sliding Vane",
                "Rotary Sliding Vane",
                "Water compressors",
                "Rotary Scroll Air Compressors",
                "Two-rotator compressors"));//banner_rotary_sliding_vain

        myDb.AirCompQuestion(new Question(
                "14",
                "Identify the image",
                "Centrifugal Air Compressors",
                "Centrifugal Air Compressors",
                "Rotary Sliding Vane",
                "Two-rotator compressors",
                "Water compressors"));//banner_centrifugal


        myDb.AirCompQuestion(new Question(
                "15",
                "Identify the image",
                "Rocking piston Compressors",
                "Rocking piston Compressors",
                "Two-rotator compressors",
                "Water compressors",
                "Rotary Sliding Vane"));//banner_rockingpiston1


        myDb.AirCompQuestion(new Question(
                "16",
                "Identify the image",
                "Multi-Stage Compressors",
                "Multi-Stage Compressors",
                "Two-rotator compressors",
                "Water compressors",
                "Rotary Sliding Vane"));//banner_multi_stage_compressor

        myDb.AirCompQuestion(new Question(
                "17",
                "Identify the image",
                "AfterCooler",
                "AfterCooler",
                "Multi-Stage Compressors",
                "Water compressors",
                "Rotary Sliding Vane"));//banner_aftercooler

        myDb.AirCompQuestion(new Question(
                "18",
                "Identify the image",
                "Intercooler",
                "Intercooler",
                "Smoke Compressors",
                "Water compressors",
                "Rotary Sliding Vane"));//banner_intercooler

    }


    public void AirDistributionQues() {
        myDb.AirDistQuestion(new Question(
                "1",
                "If the compression ratio of a system with a reciprocating compressor decreases, the amount of refrigerant pumped by the compressor ____.",
                "will increase",
                "will increase",
                "will decrease",
                "will remain the same",
                "cannot be determined"));//2

        myDb.AirDistQuestion(new Question(
                "2",
                "If the suction valve in a reciprocating compressor is leaking slightly, the ____.",
                "Cooling capacity will be reduced",
                "Cooling capacity will be reduced",
                "Flow rate of refrigerant through the system will increase",
                "Compressor can overheat",
                "Both a and c"));//3

        myDb.AirDistQuestion(new Question(
                "3",
                "The clearance volume of a compressor ______.",
                "increases as the compression ratio increases",
                "increases as the compression ratio increases",
                "increases as the compression ratio decreases",
                "decreases as the compression ratio increases",
                "remains the same as the compression ration decreases"));//4

        myDb.AirDistQuestion(new Question(
                "4",
                "What could happen if a semi-hermetic compressor is operated with the suction service valve backseated and the discharge valve frontseated?",
                "The compressor capacity will increase.",
                "The system will pump down.",
                "The system will pump out.",
                "The head of the compressor may blow off.",
                "The compressor capacity will increase."));//5

        myDb.AirDistQuestion(new Question(
                "5",
                "Liquid entering the compression chamber of a reciprocating compressor can lead to ____.",
                "both a and b",
                "damaged valves",
                "damaged piston rings",
                "damaged expansion valves",
                "both a and b"));//6
        myDb.AirDistQuestion(new Question(
                "6",
                "Most of the heat energy in the suction vapor entering the compressor is ____.",
                "latent heat",
                "heat of compression",
                "sensible heat",
                "latent heat",
                "defrost heat"));//7


        myDb.AirDistQuestion(new Question(
                "7",
                "The temperature of the compressed gas leaving the compressor can best be described as ____.",
                "hot",
                "cold",
                "hot",
                "warm",
                "cool"));//8

        myDb.AirDistQuestion(new Question(
                "8",
                "The sight glass on a semi-hermetic compressor is used to allow light to enter the crankcase to keep the oil warm.",
                "False",
                "True",
                "False",
                "",
                ""));//9

        myDb.AirDistQuestion(new Question(
                "9",
                "The shell of a hermetic type reciprocating compressor is in the low pressure side of the system.",
                "True",
                "True",
                "False",
                "",
                ""));//11

        myDb.AirDistQuestion(new Question(
                "10",
                "The hermetic reciprocating compressor motor is essentially made of the same materials as an open type compressor motor.\n",
                "False",
                "True",
                "False",
                "",
                ""));


        //Images

        myDb.AirDistQuestion(new Question(
                "11",
                "Identify the image",
                "CENTRIFUGAL FAN",
                "CENTRIFUGAL FAN",
                "AXIAL FAN",
                "DAMPER",
                ""));

        myDb.AirDistQuestion(new Question(
                "12",
                "Identify the image",
                "AXIAL FAN",
                "CENTRIFUGAL FAN",
                "AXIAL FAN",
                "DAMPER",
                ""));


        myDb.AirDistQuestion(new Question(
                "13",
                "Identify the image",
                "VANE AXIAL",
                "VANE AXIAL",
                "AXIAL FAN",
                "DAMPER",
                ""));


        myDb.AirDistQuestion(new Question(
                "14",
                "Identify the image",
                "TUBE AXIAL",
                "CENTRIFUGAL FAN",
                "AXIAL FAN",
                "TUBE AXIAL",
                ""));

        myDb.AirDistQuestion(new Question(
                "15",
                "Identify the image",
                "PROPELLER",
                "PROPELLER",
                "VANE AXIAL",
                "AXIAL FAN",
                ""));
    }


    public void ReciprocatingCompressorQuestion() {

        myDb.ReciprocatinQues(new Question(
                "1",
                "Water jacket plugs:",
                "are also known as core plugs.",
                "are designed to be quick drain-plugs for the cooling system. ",
                "are also known as core plugs.",
                "are usually made of hardened steel or aluminum.",
                "are also known as cylinder plugs."));

        myDb.ReciprocatinQues(new Question(
                "2",
                "Technician A says it is not uncommon for a piston ring or even a piston skirt to break when worn pistons are being forced out. Technician B says use a long dowel or hammer handle to tap a piston out of the cylinder. Who is correct?",
                "Both Technician A and Technician B",
                "Technician A",
                "Technician B",
                "Both Technician A and Technician B",
                "Neither Technician A nor Technician B"));

        myDb.ReciprocatinQues(new Question(
                "3",
                "The quickest tool to measure a cylinder bore is a(n):",
                "Dial bore gauge.",
                "Inside micrometer",
                "Dial bore gauge.",
                "Dial indicator.",
                "Vernier caliper."));

        myDb.ReciprocatinQues(new Question(
                "4",
                "The engine may have to be removed from the engine stand to:",
                "Remove the rear oil galley plugs.",
                "Remove the front oil galley plugs. ",
                "remove the front water jacket core plugs.",
                "Remove the rear oil galley plugs.",
                "Remove the side water jacket core plugs."));

        myDb.ReciprocatinQues(new Question(
                "5",
                "Technician A says the harmonic balancer (crankshaft damper) reduces vibration from the piston and crankshaft assembly. Technician B says crankshaft seals are design to contain lubricating oil. Who is correct?",
                "Both Technician A and Technician B",
                "Technician A",
                "Technician B",
                "Both Technician A and Technician B",
                "Neither Technician A nor Technician B"));

        myDb.ReciprocatinQues(new Question(
                "6",
                "Which of the following is used to enhance the longevity of the compression rings?",
                "All of the answers listed",
                "Molybdenum",
                "Nitride",
                "Chromium",
                "All of the answers listed"));

        myDb.ReciprocatinQues(new Question(
                "7",
                "All of the following are types of cylinder sleeves, EXCEPT:",
                "Articulated sleeve. ",
                "Dry flanged sleeve.",
                "Dry flanged sleeve.",
                "Articulated sleeve.",
                "Dry sleeve."));

        myDb.ReciprocatinQues(new Question(
                "8",
                "Technician A says crankshafts are internally balanced using counterweights. Technician B says crankshafts are externally balanced using counterweights. Who is correct?",
                "Both Technician A and Technician B",
                "Technician A",
                "Technician B",
                "Both Technician A and Technician B",
                "Neither Technician A nor Technician B"));

        myDb.ReciprocatinQues(new Question(
                "9",
                "Complete engine removal and disassembly is usually performed when:",
                "the engine requires major repairs or rebuilding.",
                "the engine is having a new timing belt installed.",
                "the engine is receiving a new oil pump.",
                "the engine needs to have a new oil pan gasket installed.",
                "the engine requires major repairs or rebuilding."));

        myDb.ReciprocatinQues(new Question(
                "10",
                "Technician A says most shops use a spray wash cabinet to degrease their engine blocks. Technician B says some shops dip their major engine parts in a hot or cold caustic solution for deeper cleaning. Who is correct?",
                "Both Technician A and Technician B",
                "Technician A",
                "Technician B",
                "Both Technician A and Technician B",
                "Neither Technician A nor Technician B"));

        myDb.ReciprocatinQues(new Question(
                "11",
                "Which of the following determines the stroke of a piston?",
                "Crankshaft rod throw",
                "Connecting rod journal diameter",
                "Crankshaft rod throw",
                "Crankshaft journal diameter",
                "None of the above"));

        ///images
        myDb.ReciprocatinQues(new Question(
                "12",
                "Identify the image",
                "Cylinder block",
                "Cylinder block",
                "Piston",
                "Bearing",
                "Rod"));

        myDb.ReciprocatinQues(new Question(
                "13",
                "Identify the image",
                "Cylinder head",
                "Cylinder head",
                "Cylinder block",
                "Connecting rod",
                "Single tube exchanger"));

        myDb.ReciprocatinQues(new Question(
                "14",
                "Identify the image",
                "Piston",
                "Piston",
                "Air compressor",
                "Air pump",
                "Piston ring"));

        myDb.ReciprocatinQues(new Question(
                "15",
                "Identify the image",
                "Piston Rings",
                "Piston Rings",
                "Connecting rod",
                "Bearings",
                "Piston"));


        myDb.ReciprocatinQues(new Question(
                "16",
                "Identify the image",
                "Connecting Rod",
                "Connecting Rod",
                "Screw driver",
                "Rotary screw",
                "Flat type exchanger"));

        myDb.ReciprocatinQues(new Question(
                "17",
                "Identify the image",
                "Piston Pin",
                "Piston Pin",
                "Tube exchanger",
                "Valves",
                "Suction valve"));

        myDb.ReciprocatinQues(new Question(
                "18",
                "Identify the image",
                "Safety Valve",
                "Safety Valve",
                "Relief valve",
                "Pressure relief valve",
                "Discharge valve"));

        myDb.ReciprocatinQues(new Question(
                "19",
                "Identify the image",
                "Discharge Valve",
                "Bursting disc",
                "Oil pump",
                "Intercooler",
                "Suction valve"));
    }


    public void heatExchangerQuestion() {

        myDb.HeatExchangeQuestion(new Question(
                "1",
                "A water regulating valve helps to maintain a system's head pressure by modulating the flow of refrigerant through the condenser.",
                "False",
                "True",
                "False",
                "",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "2",
                "Evaporative condensers operate more efficiently in climates where the wet bulb temperature is a great deal lower than the dry bulb temperature.\n",
                "True",
                "True",
                "False",
                "",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "3",
                "The desired water flow through a condenser connected to a recirculating water system is 3 gallons per minute, per ton of air conditioning.\n",
                "True",
                "True",
                "False",
                "",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "4",
                "If the spring pressure of a water regulating valve is increased, the system head pressure will also increase.",
                "True",
                "True",
                "False",
                "",
                ""));


        myDb.HeatExchangeQuestion(new Question(
                "5",
                "One large problem designer's face when designing air-cooled refrigeration and air conditioning equipment is a changing ambient from the changing seasons.\n",
                "True",
                "True",
                "False",
                "",
                ""));


        myDb.HeatExchangeQuestion(new Question(
                "6",
                "High head pressures from low ambient can cause insufficient refrigerant flow rates through metering devices, which in turn starve evaporators.\n",
                "False",
                "True",
                "False",
                "",
                ""));


        myDb.HeatExchangeQuestion(new Question(
                "7",
                "Low suction pressures, iced coils, short-cycling, and inefficient cooling can also result from low head pressures.\n",
                "True",
                "True",
                "False",
                "",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "8",
                "Hard starting may also occur in a low ambient condition causing low head pressures.\n",
                "True",
                "True",
                "False",
                "",
                ""));


        //multiple choices

        myDb.HeatExchangeQuestion(new Question(
                "9",
                "WHAT IS A HEAT EXCHANGER?",
                "A heat exchanger is a device designed to efficiently transfer or \"exchange\" heat from one matter to another.",
                "A heat exchanger is a device designed to efficiently transfer or \"exchange\" heat from one matter to another.",
                "It provides surface in a form which is relatively easy to construct in a wide range of sizes.",
                "It provides this surface in a form which is relatively easy to construct in a wide range of sizes",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "10",
                "WHAT IS THE USED OF FRESH WATER COOLER?",
                "Fresh water is used in a closed circuit to cool down the engine room machineries. The fresh water returning from the heat exchanger after cooling the machineries is further cooled by sea water in a sea water cooler.",
                "Fresh water is used in a closed circuit to cool down the engine room machineries. The fresh water returning from the heat exchanger after cooling the machineries is further cooled by sea water in a sea water cooler.",
                "The compressed air after coolers provides two necessary functions that provide the conditioning of compressed air.",
                "The main purpose is to avoid any damage to the cargo or perishable material so that the cargo in transported in good and healthy condition. ",
                ""));


        myDb.HeatExchangeQuestion(new Question(
                "11",
                "A heat-transfer device used for condensing steam to water by removal of the latent heat of steam and its subsequent absorption in a heat-receiving fluid, usually water, but on occasion air or a process fluid",
                "STEAM CONDENSER",
                "STEAM CONDENSER",
                "COMPRESSED AIR COOLER",
                "FRESH WATER COOLER",
                ""));


        myDb.HeatExchangeQuestion(new Question(
                "12",
                "An exchanger that consists of thin plates joined together, with a small amount of space between each plate, typically maintained by a small rubber gasket.",
                "FLAT- PLATE EXCHANGER",
                "LUBRICATING OIL COOLERS",
                "SHELL AND TUBE EXCHANGER",
                "FLAT- PLATE EXCHANGER",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "13",
                "Water- cooled coolers come in shell and tube type which allows the air to pass through the tubes while letting the water flow in the shell from the opposite direction. ",
                "WATER COOLED COOLER",
                "FRESH WATER COOLER",
                "WATER COOLED COOLER",
                "SEA WATER COOLER",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "14",
                "Is a heat exchanger that must have a safety relief valve. Always install a safety relief valve if a fuel oil volume can be shut up and heated.\n",
                "FUEL OIL HEATER",
                "FUEL OIL HEATER",
                "SEAWATER OIL HEATER",
                "FRESH WATER HEATER",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "15",
                "Provides two necessary functions that provide the conditioning of compressed air",
                "COMPRESSED AIR COOLER",
                "STEAM CONDENSER",
                "COMPRESSED AIR COOLER",
                "FRESH WATER COOLER",
                ""));

        myDb.HeatExchangeQuestion(new Question(
                "16",
                "These are the most widespread and commonly used basic heat exchanger configuration in the process industries. ",
                "SHELL AND TUBE EXCHANGER",
                "LUBRICATING OIL COOLERS",
                "SHELL AND TUBE EXCHANGER",
                "FLAT- PLATE EXCHANGER",
                ""));

    }

    public void heatEngineQuestion() {
        myDb.HeatEngineQuestion(new Question(
                "1",
                "A heat engine takes in 900 J of heat from a high temperature reservoir and produces 300 J of work in each cycle. What is its efficiency?",
                "33%",
                "11%",
                "12.6%",
                "25%",
                "33%"));

        myDb.HeatEngineQuestion(new Question(
                "2",
                "A heat engine takes in 700 J of heat from a high temperature reservoir and rejects 500 J of heat to a lower temperature reservoir. How much work does the engine do in each cycle? ",
                "200J",
                "100J",
                "200J",
                "250J",
                "300J"));

        myDb.HeatEngineQuestion(new Question(
                "3",
                "Calculate the efficiency of the engine in the previous problem",
                "29%",
                "10%",
                "23%",
                "25%",
                "29%"));

        myDb.HeatEngineQuestion(new Question(
                "4",
                "An engine that has an efficiency of 25% takes in 200 J of heat during each cycle. Calculate the amount of work this engine performs. ",
                "50J",
                "20J",
                "30J",
                "50J",
                "15J"));


        myDb.HeatEngineQuestion(new Question(
                "5",
                "Calculate the efficiency of a Carnot engine operating between temperatures of 900 K and 300K.",
                "67%",
                "57%",
                "67%",
                "59%",
                "69%"));


        myDb.HeatEngineQuestion(new Question(
                "6",
                "Calculate the efficiency of a Carnot engine operating between temperatures of 400o C and 100o C.",
                "44.6%",
                "44.6%",
                "26.3%",
                "34%",
                "87%"));

        myDb.HeatEngineQuestion(new Question(
                "7",
                "What would be the efficiency of a Carnot engine operating with boiling water as one reservoir and a freezing mixture of ice and water as the other reservoir?   ",
                "27%",
                "27%",
                "26%",
                "37%",
                "36%"));

        myDb.HeatEngineQuestion(new Question(
                "8",
                "From time to time people suggest using the difference in the temperature of water at the surface of the ocean and that near the bottom of the ocean for operating a heat engine. Using 20o C as the high temperature and 4o C as the low temperature what is the efficiency of such a device?",
                "5.5%",
                "2.2%",
                "3.3%",
                "4.4%",
                "5.5%"));

        myDb.HeatEngineQuestion(new Question(
                "9",
                "A refrigerator uses 400 J of work to remove 200 J of heat from its contents. How much heat must it reject to its surroundings? ",
                "600J",
                "200J",
                "300J",
                "500J",
                "600J"));

    }


    public void storageCompressAirQuestion() {
        myDb.StorageCompressAirQuestion(new Question(
                "1",
                "The purpose of _____________ is to help manage the supply of electricity in the grid",
                "Storage of Compressed Air",
                "Energy Compressor",
                "Compressed Air",
                "Storage of Compressed Air",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "2",
                "Retains the heat from compression and re-uses this when the air is expanded to produce the power",
                "Adiabatic Storage",
                "Diabetic Storage",
                "Adiabatic Storage",
                "Isothermal Storage",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "3",
                "It takes the heat and dissipates it into the atmosphere via heat intercoolers.",
                "Diabetic Storage",
                "Diabetic Storage",
                "Adiabatic Storage",
                "Isothermal Storage",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "4",
                "Involves using heat exchangers to try to always keep the internal and external temperatures the same, so as the air is compressed heat dissipates into the atmosphere.",
                "Isothermal Storage",
                "Diabetic Storage",
                "Adiabatic Storage",
                "Isothermal Storage",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "5",
                "It allows proper fuel flow into the fuel tank during refueling.",
                "Inlet Valve",
                "Inlet Valve",
                "Relief Valve",
                "Outlet Valve",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "6",
                "It is a safety device designed to protect a pressurized vessel or system during an overpressure event.",
                "Pressure Relief Valve",
                "Pressure Relief Valve",
                "Inlet Valve",
                "Outlet Valve",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "7",
                "Connect this Valve as direct and close as possible to the vessel being protected.",
                "Inlet Piping",
                "Relief Valve",
                "Inlet Piping",
                "Outlet Valve",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "8",
                "This must be drained properly to prevent the accumulation of liquids on the downstream side of the safety Valve",
                "Discharge Piping",
                "Discharge Piping",
                "Inlet Piping",
                "Outlet Valve",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "9",
                "It is a copper disc provided at the air cooler of the compressor.",
                "Bursting Disc",
                "Discharge Piping",
                "Fusible Plug",
                "Bursting Disc",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "10",
                "Generally located on the discharge side of the compressor, it fuses if the air temperature is higher than the operational temperature.",
                "Fusible Plug",
                "Inlet Valve",
                "Fusible Plug",
                "Bursting Disc",
                ""));


        //images

        myDb.StorageCompressAirQuestion(new Question(
                "11",
                "",
                "SPRING LOADED RELIEF VALVE",
                "PRESSURE RELIEF VALVE",
                "SPRING LOADED RELIEF VALVE",
                "INLET VALVE",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "12",
                "",
                "PRESSURE RELIEF VALVE",
                "PRESSURE RELIEF VALVE",
                "SPRING LOADED RELIEF VALVE",
                "INLET VALVE",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "13",
                "",
                "INLET VALVE",
                "PRESSURE RELIEF VALVE",
                "SPRING LOADED RELIEF VALVE",
                "INLET VALVE",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "14",
                "",
                "BURSTING DISC",
                "BURSTING DISC",
                "SPRING LOADED RELIEF VALVE",
                "INLET VALVE",
                ""));

        myDb.StorageCompressAirQuestion(new Question(
                "15",
                "",
                "DRAIN VALVE",
                "BURSTING DISC",
                "DRAIN VALVE",
                "INLET VALVE",
                ""));
    }

    public void operatingPumpingQuestion() {
        myDb.OperatingPumpSystemQuestion(new Question(
                "1",
                "WHAT IS A PUMP?",
                "The pump is a machine which has the function of increasing the total energy of a liquid; this means that the pump transfers energy to the fluid that it receives from the driving motor.",
                "The pump is a machine which has the function of increasing the total energy of a liquid; this means that the pump transfers energy to the fluid that it receives from the driving motor.",
                "The pump is the force that pushes the liquid away from the center.",
                "A mechanical device using suction or pressure to raise or move liquids, compress gases, or force air into inflatable objects such as tires.",
                ""));

        myDb.OperatingPumpSystemQuestion(new Question(
                "2",
                "WHAT IS A CENTRIFUGAL PUMP?",
                "The pump is the force that pushes the liquid away from the center.",
                "The pump is the force that pushes the liquid away from the center.",
                "The process in which the impeller of a centrifugal pump will get fully sub merged in liquid without any air trap inside.",
                "Used to pump a liquid from lower pressure area to a High pressure area. ",
                ""));


        myDb.OperatingPumpSystemQuestion(new Question(
                "3",
                "WHAT IS AN IMPELLER?",
                "It vanes that push the liquid through the impeller.",
                "It vanes that push the liquid through the impeller.",
                "Impeller is fitted inside the casing",
                "It transmits the torque/power. And supporting the impeller and other rotating parts.",
                ""));

        myDb.OperatingPumpSystemQuestion(new Question(
                "4",
                "WHAT IS THE PRINCIPLE Of ROTARY DISPLACEMENT PUMPS?",
                "A cylinder in which piston works. the movement of piston is obtained by a connecting rod, which connects the piston and the rotating crank.",
                "It vanes that push the liquid through the impeller. ",
                "A cylinder in which piston works. the movement of piston is obtained by a connecting rod, which connects the piston and the rotating crank.",
                "It transmits the torque/power. And supporting the impeller and other rotating parts.",
                ""));

        myDb.OperatingPumpSystemQuestion(new Question(
                "5",
                "WHAT IS A SHAFT?",
                "It transmits the torque/power. And supporting the impeller and other rotating parts.",
                "It vanes that push the liquid through the impeller.",
                "Impeller is fitted inside the casing",
                "It transmits the torque/power. And supporting the impeller and other rotating parts.",
                ""));

        myDb.OperatingPumpSystemQuestion(new Question(
                "6",
                "WHAT IS THE NEED OF A PUMP?",
                "To move liquid from lower elevation to higher elevation.",
                "To move liquid from lower elevation to higher elevation.",
                "Impeller is fitted inside the casing.",
                "Increase in energy is converted to a gain in Pressure Energy when the liquid is allowed to pass through an increased area.",
                ""));

        myDb.OperatingPumpSystemQuestion(new Question(
                "7",
                "WHAT IS THE AXIAL FLOW PUMP?",
                "They discharge fluid nearly axially, pumping the liquid in a direction that is parallel to the pump shaft.",
                "They discharge fluid nearly axially, pumping the liquid in a direction that is parallel to the pump shaft.",
                "Impeller is fitted inside the casing.",
                "Increase in energy is converted to a gain in Pressure Energy when the liquid is allowed to pass through an increased area.",
                ""));

    }


    private void openDB() {
        myDb = new QUIZDBAdapter(this);
        myDb.open();
    }

    private void closeDB() {
        myDb.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        AuxMachAssesmentActivity.this.finish();
        super.onBackPressed();

    }


}
