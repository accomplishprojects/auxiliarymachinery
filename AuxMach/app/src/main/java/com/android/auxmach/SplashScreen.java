package com.android.auxmach;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.auxmach.activity.Activity_Dashboard;

/**
 * Created by luigigo on 2/23/16.
 */
public class SplashScreen extends Activity {
    private static long SLEEP_TIME = 2;
    private static String TAG = SplashScreen.class.getName();
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        Typeface ironmanTF = Typeface.createFromAsset(getAssets(), "fonts/ironman.ttf");
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setTypeface(ironmanTF);
        IntentLauncher launcher = new IntentLauncher();
        launcher.start();
    }

    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 2000);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            Intent mainDrawerIntent = new Intent(getApplicationContext(), Activity_Dashboard.class);
            startActivity(mainDrawerIntent);
            SplashScreen.this.finish();
        }
    }
}
