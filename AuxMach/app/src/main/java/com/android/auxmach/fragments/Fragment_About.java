package com.android.auxmach.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.auxmach.R;
import com.android.auxmach.fragments.topics.Fragment_Guides;
import com.android.auxmach.utilities.tools.CustomUtils;

/**
 * Created by luigigo on 3/6/16.
 */
public class Fragment_About extends Fragment {

    View mainView;
    String strHeader, strDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_about, container, false);


        String[] listAbout = new String[]{"Researchers", "Course Description", "Guides"};
        ListView lvAbout = (ListView) mainView.findViewById(R.id.lvAbout);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item_about, listAbout);
        lvAbout.setAdapter(adapter);
        lvAbout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {
                    case 0:
                        strHeader = "Researchers";
                        strDetails = "Technological Institute of the Philippines" +
                                "\n\nLead Programmer:\n" + "Albea Antoinette Galit" +
                                "\n\nExecutive Documentary:\n" + "Ella Joy Bobiles" +
                                "\n\nAnalyst:\n" + "Jerome Secillano";
                        showDialog(strHeader, strDetails);
                        break;

                    case 1:
                        strHeader = "Course Description";
                        strDetails = "TOPIC DESCRIPTION:" + "\n(Auxiliary Machinery)\n" +
                                "All machinery devices which form the non-propulsive equipment is auxiliary machinery\n" +
                                "\nPURPOSE:\nTo serve the main engine the necessities of the ship\n" +
                                "\n\nFUNCTIONS:\n" +
                                "- To supply the needs of the main engines and boilers\n" +
                                "- To keep the ship dry and trimmed\n" +
                                "- To supply domestic needs\n" +
                                "- To apply the main power of the engines\n" +
                                "- To supply the ship with electric power and lightning\n" +
                                "- To provide for safety";
                        showDialog(strHeader, strDetails);
                        break;

                    case 2:
                        strHeader = "Guides";
                        showGuidesDialog(strHeader);
                        break;
                }
            }
        });

        return mainView;
    }


    public void showDialog(String strHeader, String strDetails) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_about);
        dialog.show();

        TextView txtHeader = (TextView) dialog.findViewById(R.id.txtHeader);
        TextView txtDetails = (TextView) dialog.findViewById(R.id.txtDetails);
        txtHeader.setText(strHeader);
        txtDetails.setText(strDetails);

        Button btnOkDialog = (Button) dialog.findViewById(R.id.btnOkDialog);
        btnOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public void showGuidesDialog(String strHeader) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_guides);
        TextView txtHeader = (TextView) dialog.findViewById(R.id.txtHeader);
        txtHeader.setText(strHeader);
        final ListView listView = (ListView) dialog.findViewById(R.id.lvList);
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.list_item_about,
                new String[]{"Dashboard", "Side Menu", "Topic", "Assessment", "Search Topics", "Topic Viewer", "3D Object Viewer"}));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {


                    case 0:
                        Fragment_Guides.strHeader = "Dashboard";
                        Fragment_Guides.strDescription = "It is the first screen that shows upon entering the application.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 1:
                        Fragment_Guides.strHeader = "Side Menu";
                        Fragment_Guides.strDescription = "Using this drawer user can access topics and 3D object viewer of the app.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 2:
                        Fragment_Guides.strHeader = "Topics";
                        Fragment_Guides.strDescription = "Within this screen user can view topics, assessments and 3D object viewer of the app.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 3:
                        Fragment_Guides.strHeader = "Assessment";
                        Fragment_Guides.strDescription = "In this activity user can take quizzes of the selected topic.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 4:
                        Fragment_Guides.strHeader = "Search Topics";
                        Fragment_Guides.strDescription = "In this activity user can search specific topics.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 5:
                        Fragment_Guides.strHeader = "Topic Viewer";
                        Fragment_Guides.strDescription = "Within this screen user can view the searched topic.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;

                    case 6:
                        Fragment_Guides.strHeader = "3D Object Viewer";
                        Fragment_Guides.strDescription = "With this activity you can view 3D Object Models.";
                        Fragment_Guides.page = i;
                        CustomUtils.loadFragment(new Fragment_Guides(), true, getActivity());
                        dialog.dismiss();
                        break;
                }

            }
        });
        dialog.show();
    }
}
