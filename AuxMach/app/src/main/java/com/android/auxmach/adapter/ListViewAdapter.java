package com.android.auxmach.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.auxmach.R;
import com.android.auxmach.fragments.topics.Fragment_Topic_Preview;
import com.android.auxmach.model.SearchEngineModel;
import com.android.auxmach.utilities.tools.CustomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by luigigo on 3/1/16.
 */
public class ListViewAdapter extends BaseAdapter {

    List<SearchEngineModel> searchEngineModels;
    ArrayList<SearchEngineModel> arrayList, tempData;
    LayoutInflater inflater;
    Context con;

    public ListViewAdapter(Context context, ArrayList<SearchEngineModel> searchEngineModels) {
        con = context;
        this.arrayList = arrayList;
        this.tempData = arrayList;

        con = context;
        this.searchEngineModels = searchEngineModels;
        this.arrayList = new ArrayList<SearchEngineModel>();
        this.arrayList.addAll(searchEngineModels);

    }

    @Override
    public int getCount() {
        return this.searchEngineModels.size();
    }

    @Override
    public Object getItem(int i) {
        return searchEngineModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder vh;

        if (view == null) {
            vh = new ViewHolder();
            inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_search, viewGroup, false);


            vh.txtCategory = (TextView) view.findViewById(R.id.txtCategory);
            vh.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            vh.txtContent = (TextView) view.findViewById(R.id.txtContents);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        // vh.txtCategory.setText(searchEngineModels.get(i).getCategory());
        vh.txtTitle.setText(searchEngineModels.get(i).getTitle());
        vh.txtContent.setText(searchEngineModels.get(i).getContent());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle data = new Bundle();
                data.putString("category", searchEngineModels.get(i).getCategory());
                data.putString("title", searchEngineModels.get(i).getTitle());
                data.putString("content", searchEngineModels.get(i).getContent());
                data.putString("image", searchEngineModels.get(i).getImageBanner());

                Fragment fragment = new Fragment_Topic_Preview();
                fragment.setArguments(data);
                CustomUtils.loadFragment(fragment, true, con);
            }
        });

        return view;
    }

    public class ViewHolder {
        TextView txtCategory, txtTitle, txtContent;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchEngineModels.clear();
        if (charText.length() == 0) {
            searchEngineModels.addAll(arrayList);
        } else {
            for (SearchEngineModel se : arrayList) {
                if (se.getCategory().toLowerCase(Locale.getDefault()).contains(charText) || se.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    searchEngineModels.add(se);
                }
            }
        }
        notifyDataSetChanged();
    }
}
