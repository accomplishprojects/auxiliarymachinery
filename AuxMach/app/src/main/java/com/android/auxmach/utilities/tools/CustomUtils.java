package com.android.auxmach.utilities.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.android.auxmach.R;

import java.io.Serializable;

/**
 * Created by luigigo on 2/23/16.
 */
public class CustomUtils implements Serializable {
    public static String questionCategory;
    public static ProgressDialog mProgressDialog;

    public static String getQuestionCategory() {
        return questionCategory;
    }

    public static void setQuestionCategory(String questionCategory) {
        CustomUtils.questionCategory = questionCategory;
    }


    public static void loadFragment(final android.support.v4.app.Fragment fragment, boolean addToBackStack, Context context) {
        final FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_content, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
//        CustomUtils.popBackStack(fragment);
        fragmentTransaction.commit();
    }

    public static void checkTTSdata(Activity activity) {
        //check for TTS data
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        activity.startActivityForResult(checkTTSIntent, 0);
    }

    /**
     * Display progress dialog
     *
     * @param context
     */
    public static void showDialog(Context context) {
        mProgressDialog = new ProgressDialog(context);
        // Set progressdialog title
        mProgressDialog.setTitle("Loading");
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    /**
     * Hide Dialog
     */
    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
        mProgressDialog.setCancelable(true);
//        mProgressDialog.cancel();
    }
}
